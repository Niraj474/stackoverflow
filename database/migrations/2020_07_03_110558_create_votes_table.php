<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('vote_id');
            $table->string('vote_type');
            $table->tinyInteger('vote');
            $table->timestamps();

            //vote_id can be question ka id or can be answer ka id depends on vote_type
            // so there can be a case where user had voted to question having id as 1 and same user had voted to answer having id as 1 so we can get the difference with the help of vote_type
            // so these three columns must be unique.
            $table->unique(['user_id', 'vote_id', 'vote_type']);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votes');
    }
}
