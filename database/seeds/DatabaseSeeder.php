<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

        /*
        factory(\App\User::class, 5)->create()->each(function ($user) {
            //this body will run for every user that has been created!
            // create will return a collection and we can apply each() on collection.
            for ($i = 1; $i <= rand(5, 10); $i++) {
                $user->questions()->create(factory(\App\Question::class)->make()->toArray());
                // create method requires an array(Model class)
                // using factory we will get an object
                // create method of factory directly stores the record in database.
                // we have to use make method bcoz we dont wanna save that directly in database.we have to insert using relationship i.e.$user->questions()->create().
                // make method returns an object so we are writing toArray after that so that we can get an array which we have to pass in create method(Model class).
            }
        });
        */

        factory(\App\User::class, 5)->create()
            ->each(function ($user) {
                $user->questions()
                    ->saveMany(factory(\App\Question::class, rand(2, 5))->make())
                    ->each(function ($question) {
                        $question->answers()
                            ->saveMany(factory(\App\Answer::class, rand(2, 7))->make());
                    });
            });
    }
}
