@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @include('layouts.partials._message')
            <div class="d-flex justify-content-end mb-3">
                <a href="{{ route('questions.create') }}" class="btn btn-outline-primary">Ask a Question</a>
            </div>
            <div class="card">
                <div class="card-header">All Questions</div>
                @foreach ($questions as $question)
                    <div class="card-body">
                        <div class="media">
                            <div class="d-flex flex-column statistics mr-3">
                                <div class="votes text-center mb-3">
                                <strong class="d-block">{{ $question->votes_count }}</strong>
                                Votes
                                </div>
                                <div class="answers text-center mb-3">
                                <strong class="d-block answers {{ $question->answers_style }}">
                                        {{ $question->answers_count }}
                                    </strong>
                                    Answers
                                </div>
                                <div class="views text-center">
                                    <strong class="d-block">
                                        {{ $question->views_count }}
                                    </strong>
                                Views
                                </div>
                            </div>
                            <div class="media-body">
                            <div class="d-flex justify-content-between">
                                <h4>
                                    <a href="{{ $question->url }}">{{ $question->title }}</a>
                                    @if ($question->is_favourite)
                                        <i class="fa fa-star text-warning"></i>
                                    @endif
                                </h4>
                                <div class="buttons d-flex">

                                        @can('update', $question)

                                            {{-- @can will call specified method of QuestionsPolicy with Question object and based of the result it will show the edit button --}}
                                            <div class="">
                                                <a href="{{ route('questions.edit',$question->id) }}" class="btn btn-outline-info mr-2">Edit</a>
                                            </div>
                                        @endcan
                                        @can('delete', $question)
                                            <form action="{{ route('questions.destroy',$question->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" onclick="return confirm('Are u sure u want to delete?');" class="btn btn-outline-danger">Delete</button>
                                            </form>
                                        @endcan
                                </div>
                            </div>
                            <p>
                                Asked By: <a href="#">{{ $question->owner->name }}</a>
                            <span class="text-muted"> | Asked : {{ $question->created_date }}</span>
                            </p>
                            <p>{{ strip_tags(Str::limit($question->body,250)) }}</p>
                            {{-- <p>Niraj</p><p>Bathija</p>  -> strip_tags will give like this NirajBathija it will not show it like html means it will not show heading text like headings and all other styles.strip_tags returns normal string and it just removes the tags between text. --}}
                            </div>
                        </div>
                    </div>
                    <hr>
                @endforeach
                <div class="card-footer">
                    {{ $questions->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
