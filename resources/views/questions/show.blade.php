@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.min.css">
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.min.js"></script>
@endsection

@section('content')
    <div class="container">
        @include('layouts.partials._message')
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h1>{{ $question->title }}</h1>
                    </div>
                    <div class="card-body">
                        {!! $question->body !!}
                    </div>
                    <div class="card-footer">
                        <div class="d-flex justify-content-between mr-3">
                            <div class="d-flex">
                                <div>
                                    @can('vote', $question)
<<<<<<< HEAD
                                        <form action="{{ route('questions.vote',[$question->id,1]) }}" method="post">
                                            @csrf
                                            <button type="submit"
                                            class="btn {{ auth()->user()->hasQuestionUpVote($question) ? 'text-dark' : 'text-black-50' }}">
=======
                                        @if (!auth()->user()->hasQuestionUpVote($question))
                                            <form action="{{ route('questions.vote',[$question->id,1]) }}" method="post">
                                                @csrf
                                                <button type="submit"
                                                class="btn {{ auth()->user()->hasQuestionUpVote($question) ? 'text-dark' : 'text-black-50' }}">
                                                    <i class="fa fa-caret-up fa-3x"></i>
                                                </button>
                                            </form>
                                        @else
                                            <div class="text-dark d-flex justify-content-center mb-2">
>>>>>>> Votes-feature
                                                <i class="fa fa-caret-up fa-3x"></i>
                                            </div>
                                        @endif
                                    @endcan
                                    @guest
                                        <a href="{{ route('login') }}" title="Up Vote" class="vote-up d-block text-center text-black-50">
                                            <i class="fa fa-caret-up fa-3x"></i>
                                        </a>
                                    @endguest
                                    <div class="{{ auth()->id() === $question->user_id ? 'd-flex flex-column mt-2' : '' }}">
                                        <h4 class="text-muted text-center m-0">{{ $question->votes_count }}</h4>
                                        @if (auth()->id() === $question->user_id)
                                            <p class="text-muted lead mt-1">Votes</p>
                                        @else

                                        @endif
                                    </div>
                                    @can('vote', $question)
                                        @if (!auth()->user()->hasQuestionDownVote($question))
                                            <form action="{{ route('questions.vote',[$question->id,-1]) }}" method="post">
                                                @csrf
                                                <button type="submit"
                                                class="btn {{ auth()->user()->hasQuestionDownVote($question) ? 'text-dark' : 'text-black-50' }}">
                                                    <i class="fa fa-caret-down fa-3x"></i>
                                                </button>
                                            </form>
                                        @else
                                            <div class="text-dark d-flex justify-content-center mb-2">
                                                <i class="fa fa-caret-down fa-3x"></i>
                                            </div>
                                        @endif
                                    @endcan
                                    @guest
                                        <a href="{{ route('login') }}" title="Up Vote" class="vote-down d-block text-center text-black-50">
                                            <i class="fa fa-caret-down fa-3x"></i>
                                        </a>
                                    @endguest
                                </div>

                                <div class="ml-5 mt-2 {{ $question->is_favourite ? 'text-warning' : 'text-black-50'  }}">
                                    @can('markAsFavourite', $question)

                                        <form action="{{ route($question->is_favourite ? 'questions.unfavourite' : 'questions.favourite',$question->id) }}" method="post">

                                            @csrf

                                            @if ($question->is_favourite)
                                                @method('DELETE')
                                            @endif

                                            <button type="submit"
                                            class="btn {{ $question->is_favourite ? 'text-warning' : 'text-black-50' }}">

                                                <i class="fa {{ $question->is_favourite ? 'fa-star' : 'fa-star-o' }} fa-2x"></i>

                                            </button>
                                        </form>
                                        <h4 class="votes-count m-0 text-center text-black-50">{{ $question->favourites_count }}</h4>

                                    @else
                                        <i class="fa fa-star-o fa-2x text-black-50"></i>
                                        <h4 class="votes-count m-0 text-center text-black-50">{{ $question->favourites_count }}</h4>

                                    @endcan
                                </div>
                            </div>
                            <div class="d-flex flex-column">
                                <div class="text-muted mb-2 text-right">
                                    Asked {{ $question->created_date }}
                                </div>
                                <div class="d-flex mb-2">
                                    <div>
                                        <img src="{{ $question->owner->avatar }}" alt="">
                                    </div>
                                    <div class="mt-2 ml-2">
                                        {{ $question->owner->name }}
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--ANSWERS-->
        @include('answers._index')

        @include('answers._create')
    </div>
@endsection
