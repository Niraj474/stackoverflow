
        <div class="row mt-4">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="mt-0">
                            {{ Str::plural('Answer', $question->answers_count) }}
                        </h3>
                    </div>
                    <div class="card-body">
                        @foreach($question->answers as $answer)
                            <div class="d-flex">
                                <div class="d-flex flex-column">
                                    <div>
                                        @can('vote', $answer)
                                            @if (!auth()->user()->hasAnswerUpVote($answer))
                                                <form action="{{ route('answers.vote',[$answer->id,1]) }}" method="post">
                                                    @csrf
                                                    <button type="submit"
                                                    class="btn {{ auth()->user()->hasAnswerUpVote($answer) ? 'text-dark' : 'text-black-50' }}">
                                                        <i class="fa fa-caret-up fa-3x"></i>
                                                    </button>
                                                </form>
                                            @else
                                                <div class="d-flex justify-content-center mb-2">
                                                    <i class="fa fa-caret-up fa-3x"></i>
                                                </div>
                                            @endif
                                        @endcan
                                        @guest
                                            <a href="{{ route('login') }}" title="Up Vote" class="vote-up d-block text-center text-black-50">
                                                <i class="fa fa-caret-up fa-3x"></i>
                                            </a>
                                        @endguest
                                    <div class="{{ auth()->id() === $answer->user_id ? 'd-flex flex-column mt-2' : '' }}">
                                        <h4 class="text-muted text-center m-0">{{ $answer->votes_count }}</h4>
                                        @if (auth()->id() === $answer->user_id)
                                            <p class="text-muted lead text-center mt-1">Votes</p>
                                        @else

                                        @endif
                                    </div>
                                    @can('vote', $answer)
                                        @if (!auth()->user()->hasAnswerDownVote($answer))
                                            <form action="{{ route('answers.vote',[$answer->id,-1]) }}" method="post">
                                                @csrf
                                                <button type="submit"
                                                class="btn {{ auth()->user()->hasAnswerDownVote($answer) ? 'text-dark' : 'text-black-50' }}">
                                                    <i class="fa fa-caret-down fa-3x"></i>
                                                </button>
                                            </form>
                                        @else
                                            <div class="d-flex justify-content-center mb-2">
                                                <i class="fa fa-caret-down fa-3x"></i>
                                            </div>
                                        @endif
                                    @endcan
                                    @guest
                                        <a href="{{ route('login') }}" title="Up Vote" class="vote-down d-block text-center text-black-50">
                                            <i class="fa fa-caret-down fa-3x"></i>
                                        </a>
                                    @endguest
                                    </div>

                                    <div class="mt-2">
                                        @can('markAsBest', $answer)
                                            <form action="{{ route('answers.bestAnswer',$answer->id) }}" method="post">
                                                    @csrf
                                                    @method('PUT')
                                                <button type="submit" class="btn {{ $answer->best_answer_status }}"><i class="fa fa-check fa-2x"></i></button>
                                            </form>
                                        @else
                                            @if ($answer->is_best)
                                                <i class="fa fa-check fa-2x d-block text-success mb-3"></i>
                                            @endif
                                        @endcan
                                    </div>
                                </div>
                                <div class="ml-5">
                                    {!! $answer->body !!}
                                </div>
                            </div>
                            <div class="d-flex justify-content-between mr-3">
                                <div class="d-flex">
                                    <div class="mr-2">
                                        @can('update', $answer)
                                            <a href="{{ route('questions.answers.edit',[$question->id,$answer->id]) }}" class="btn btn-outline-info">Edit</a>
                                        @endcan
                                    </div>
                                    @can('delete', $answer)
                                        <form action="{{ route('questions.answers.destroy',[$question->id,$answer->id]) }}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-outline-danger" onclick="return confirm('Are You sure u want to delete the answer?');">Delete</button>
                                        </form>
                                    @endcan
                                </div>
                                <div class="d-flex flex-column">
                                    <div class="text-muted mb-2 text-right">
                                        Answered {{ $answer->created_date }}
                                    </div>
                                    <div class="d-flex mb-2">
                                        <div>
                                            <img src="{{ $answer->author->avatar }}" alt="">
                                        </div>
                                        <div class="mt-2 ml-2">
                                            {{ $answer->author->name }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
