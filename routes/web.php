<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/posts', function () {
    return view('post');
});

Auth::routes();

Route::resource('questions', 'QuestionsController')->except('show');
//Now if we write route('questions.show') it will not work bcoz we've removed it using except method.
// we have to manually write questions.show route bcoz we want to bind it using slug for questions.show only.
Route::get('questions/{slug}', 'QuestionsController@show')->name('questions.show');

Route::post('questions/{question}/favourite', 'FavouritesController@store')->name('questions.favourite');
Route::delete('questions/{question}/unfavourite', 'FavouritesController@destroy')->name('questions.unfavourite');

Route::post('questions/{question}/vote/{vote}', 'VotesController@voteQuestion')->name('questions.vote');
Route::post('answers/{answer}/vote/{vote}', 'VotesController@voteAnswer')->name('answers.vote');

Route::resource('questions.answers', 'AnswersController')->except(['show', 'index', 'create']);
Route::put('answers/{answer}/best-answer', 'AnswersController@bestAnswer')->name('answers.bestAnswer');

Route::get('users/notifications', 'UsersController@notifications')->name('users.notifications');

Route::get('/home', 'HomeController@index')->name('home');
