<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    //
    protected $fillable = ['user_id', 'body'];

    public static function boot()
    {
        parent::boot(); //we have to write this line bcoz opf inheritance rule

        static::created(function ($answer) {
            $answer->question->increment('answers_count');
        });
        static::deleted(function ($answer) {
            $answer->question->decrement('answers_count');
        });
    }

    /**
     * ACCESSORS
     */

    public function getCreatedDateAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    public function getBestAnswerStatusAttribute()
    {
        if ($this->id === $this->question->best_answer_id) {
            return "text-success";
        }
        return "text-dark";
    }

    public function getIsBestAttribute()
    {
        return $this->id === $this->question->best_answer_id;
    }

    /**
     * Relationship Methods
     */

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function votes()
    {
        return $this->morphToMany(User::class, 'vote')->withTimestamps();
    }

    /**
     * Helper Functions
     */

    public function vote(int $vote)
    {
        $this->votes()->attach(auth()->id(), ['vote' => $vote]);
        if ($vote < 0) {
            $this->decrement('votes_count');
        } else {
            $this->increment('votes_count');
        }
    }

    public function updateVote(int $vote)
    {
        /**
         *
         *Here we will decrement or increment by 2 times bcoz if the user has already upvoted to a question and that question has votes_count = 10 so without that user's upvote votes_count will be 9 and it that user has down-voted then votes_count should be 8.
         * same is the case for vice versa.
         */
        $this->votes()->updateExistingPivot(auth()->id(), ['vote' => $vote]);
        // pivot columns are those columns which are in bridging table but not related to some other table like user_id,question_id,etc.
        // Here pivot column is vote which is not related to any table but we need that column.
        // we can update pivot columns by using updateExistingPivotMethod
        // Here we can ensure that user has already voted to an answer and now that user wants to update his vote for that answer.
        // so we are passing user_id bcoz it is the foreign_key
        //         Updating A Record On A Pivot Table
        // If you need to update an existing row in your pivot table, you may use updateExistingPivot method. This method accepts the pivot record foreign key and an array of attributes to update.
        // $user = App\User::find(1);

        // $user->roles()->updateExistingPivot($roleId, $attributes);
        if ($vote < 0) {
            $this->decrement('votes_count');
            $this->decrement('votes_count');
        } else {
            $this->increment('votes_count');
            $this->increment('votes_count');
        }
    }
}
