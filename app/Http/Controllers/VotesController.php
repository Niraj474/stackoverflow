<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Notifications\VotesNotification;
use App\Question;
use Illuminate\Http\Request;

class VotesController extends Controller
{
    //
    public function voteQuestion(Question $question, int $vote)
    {
        // Either we need to update the vote or we need to create the vote
        // if hasVoteForQuestion returns false then we need to create the vote.
        if (auth()->user()->hasVoteForQuestion($question)) {
            $question->updateVote($vote);
        } else {
            $question->vote($vote);
        }
        $question->owner->notify(new VotesNotification($question, $vote));
        return redirect()->back();
    }

    public function voteAnswer(Answer $answer, int $vote)
    {
        // Either we need to update the vote or we need to create the vote
        // if hasVoteForQuestion returns false then we need to create the vote.
        if (auth()->user()->hasVoteForAnswer($answer)) {
            $answer->updateVote($vote);
        } else {
            $answer->vote($vote);
        }
        $answer->author->notify(new VotesNotification($answer, $vote));
        return redirect()->back();
    }
}
