<?php

namespace App\Http\Controllers;

use App\Http\Requests\Questions\CreateQuestionRequest;
use App\Http\Requests\Questions\UpdateQuestionRequest;
use App\Question;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(['create', 'store', 'edit', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::with('owner')->latest()->paginate(10); //will eager load the records.
        // with method preloads the relationship which is specified.
        // E.g. without with method
        // Here is the scenario where we want owner name of all the questions
        /**
         * Here problem is there if we do this :
         * $questions = Question::latest()->paginate(10);
         * Bcoz in questions.index we are writing $question->owner and it will run 10 times bcoz we had written paginate(10).
         * Now the problem is every time $question->owner it goes to users table and fetch the record.
         * that means if i have 25 records per page then no of queries executed will be 25+1=26.
         * 1 query for extracting the 25 records and 25 queries to get the name of the owner.
         * This problem is known as n+1 query problem and lazy loading.
         */
        return view('questions.index', compact([
            'questions'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        app('debugbar')->disable();
        return view('questions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateQuestionRequest $request)
    {
        //
        auth()->user()->questions()->create([
            'title' => $request->title,
            'body' => $request->body
        ]);

        session()->flash('success', 'Questions has been added successfully!');
        return redirect(route('questions.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        //Refer notes.txt
        $question->increment('views_count'); //this will increment the views_count every time we see any question.
        return view('questions.show', compact('question'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        //
        if ($this->authorize('update', $question)) {
            app('debugbar')->disable();
            return view('questions.edit', compact('question'));
        }
        abort(403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateQuestionRequest $request, Question $question)
    {
        //

        if ($this->authorize('update', $question)) {
            $question->update([
                'title' => $request->title,
                'body' => $request->body
            ]);

            session()->flash('success', 'Questions has been updated successfully!');
            return redirect(route('questions.index'));
        }
        abort(403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        //
        if ($this->authorize('delete', $question)) {
            $question->delete();
            session()->flash('success', 'Questions has been Deleted successfully!');
            return redirect(route('questions.index'));
        }
        abort(403);
    }
}
