<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;

class FavouritesController extends Controller
{
    //
    public function store(Question $question)
    {
        $this->authorize('markAsFavourite', $question);
        $question->favourites()->attach(auth()->id());
        session()->flash('success', "Question added to Favourites");
        return redirect()->back();
    }

    public function destroy(Question $question)
    {
        $this->authorize('markAsFavourite', $question);
        $question->favourites()->detach(auth()->id());
        session()->flash('success', "Question removed to Favourites");
        return redirect()->back();
    }
}
