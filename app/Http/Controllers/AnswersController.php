<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Http\Requests\CreateNewAnswerRequest;
use App\Http\Requests\UpdateAnswerRequest;
use App\Notifications\BestAnswerNotification;
use App\Notifications\NewReplyAdded;
use App\Question;
use Illuminate\Http\Request;

class AnswersController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateNewAnswerRequest $request, Question $question)
    {
        //
        $question->answers()->create([
            'user_id' => auth()->id(),
            'body' => $request->body
        ]);

        $question->owner->notify(new NewReplyAdded($question));
        session()->flash('success', 'Your answer has been submitted successfully');
        return redirect(route('questions.show', $question->slug));
    }

    public function edit(Question $question, Answer $answer)
    {
        $this->authorize('update', $answer);
        return view('answers._edit', compact(['answer', 'question']));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAnswerRequest $request, Question $question, Answer $answer)
    {
        //
        $answer->update([
            'body' => $request->body
        ]);
        session()->flash('success', 'Your answer has been updated successfully');
        return redirect($question->url);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question, Answer $answer)
    {
        //

        $this->authorize('markAsBest', $answer); //if authorize returns false then it automatically goes to 403.
        $answer->delete();
        session()->flash('success', 'Your answer has been Deleted successfully');
        return redirect($question->url);
    }

    public function bestAnswer(Request $request, Answer $answer)
    {
        $this->authorize('markAsBest', $answer); //if authorize returns false then it automatically goes to 403.
        $answer->question->markAsBest($answer);
        $answer->author->notify(new BestAnswerNotification($answer->question));
        return redirect()->back();
    }
}
