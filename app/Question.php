<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Question extends Model
{
    protected $fillable = ['title', 'body'];
    /**
     * Relationship Methods
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function favourites()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function votes()
    {
        return $this->morphToMany(User::class, 'vote')->withTimestamps();
    }

    /**
     * MUTATORS
     * These are special functions which have setXXXAttribute($value).
     * Here func_name is setTitleAttribute so when we set the title attribute of an object of Question this method will be automativally called and will do its work.
     */
    public function setTitleAttribute($title)
    {
        $this->attributes['title'] = $title;
        $this->attributes['slug'] = Str::slug($title);
    }

    /**
     * ACCESSORS
     * These are special functions which have getXXXAttribute().
     * Here func_name is getUrlAttribute so whereever we write $questionObj->url it will automatically call this function and return the url.
     * Note if $questionObj dont have any url then it will bring null or ''.
     */
    public function getUrlAttribute()
    {
        return "questions/{$this->slug}";
    }

    public function getCreatedDateAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    public function getAnswersStyleAttribute()
    {
        if ($this->answers_count > 0) {
            if ($this->best_answer_id) {
                return 'has-best-answer';
            }
            return 'answered';
        }
        return 'unanswered';
    }

    public function getFavouritesCountAttribute()
    {
        return $this->favourites->count();
    }

    public function getIsFavouriteAttribute()
    {
        return $this->favourites()->where(['user_id' => auth()->id()])->count() > 0;
        // this above query will search for a record in question_user where question_id = $this->id and user_id = auth()->id()
        // u can verify by writing this query in tinker
        // $question->favourites()->where('user_id','1')->toSql()
    }

    /**
     * Helper Functions
     */
    public function markAsBest(Answer $answer)
    {
        $this->best_answer_id = $answer->id;
        $this->save();
    }

    public function vote(int $vote)
    {
        $this->votes()->attach(auth()->id(), ['vote' => $vote]);
        if ($vote < 0) {
            $this->decrement('votes_count');
        } else {
            $this->increment('votes_count');
        }
    }

    public function updateVote(int $vote)
    {
        /**
         *
         *Here we will decrement or increment by 2 times bcoz if the user has already upvoted to a question and that question has votes_count = 10 so without that user's upvote votes_count will be 9 and it that user has down-voted then votes_count should be 8.
         * same is the case for vice versa.
         */
        $this->votes()->updateExistingPivot(auth()->id(), ['vote' => $vote]);
        if ($vote < 0) {
            $this->decrement('votes_count');
            $this->decrement('votes_count');
        } else {
            $this->increment('votes_count');
            $this->increment('votes_count');
        }
    }
}
